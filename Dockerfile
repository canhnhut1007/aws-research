FROM maven:3.3.9-jdk-8 as builder
WORKDIR /app
ADD . .
RUN mvn clean install -DskipTests;

FROM store/oracle/serverjre:8
#Add Source
RUN mkdir -p /var/www/demo-app
COPY --from=builder /app/target/ /var/www/demo-app/
WORKDIR /var/www/demo-app

EXPOSE 8080
ENTRYPOINT java -jar demo-app*.jar --server.port=8080